<?php

namespace kupi_othodov_ru\module_main;

use Yii;

/**
 * Description of Main
 *
 * @author nofuture17
 */
class Module extends \amd_php_dev\yii2_components\modules\Module implements \yii\base\BootstrapInterface
{
    public $theme = 'base';

    protected $_urlRules = [
        [
            'rules' => [
                '/admin' => '/main/admin/default/index'
            ],
            'append' => true,
        ],
        [
            'rules' => [
                '/mail/callback' => '/main/mail/call-back',
                '/mail/make-order' => '/main/mail/make-order',
            ],
            'append' => true,
        ],
        [
            'rules' => [
                ['class' => '\kupi_othodov_ru\module_main\urlRules\Rules']
            ],
            'append' => true,
        ],
    ];

    use \amd_php_dev\yii2_components\modules\ComposerModuleTrait;

    public function init()
    {
        parent::init();
        $this->controllerMap = \yii\helpers\ArrayHelper::merge(
            [
                'elfinder' => [
                    'class' => 'mihaildev\elfinder\Controller',
                    'layout' => '@app/views/layouts/admin',
                    'access' => ['admin'], //глобальный доступ к фаил менеджеру @ - для авторизорованных , ? - для гостей , чтоб открыть всем ['@', '?']
                    'disabledCommands' => ['netmount'], //отключение ненужных команд https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#commands
                    'roots' => [
                        [
                            'baseUrl' => '@web',
                            'basePath' => '@webroot',
                            'path' => 'data',
                            'name' => 'Global'
                        ],

                    ],
                    'managerOptions' => [
                        'validName' => '/^[^.]{1}.*$/',
                    ],
                ]
            ],
            $this->controllerMap
        );

        $this->modules = [
            'admin' => [
                'class' => '\kupi_othodov_ru\module_main\modules\admin\Module',
            ],
        ];

        \Yii::setAlias('@mainModule', __DIR__);
    }

    public function bootstrap($app)
    {
        parent::bootstrap($app);

        // если это не веб приложение, нам не нужно устанавливать тему
        if (($app instanceof \yii\web\Application)) {
            // Редиректы
            $this->_redirects($app);
            // устанавливаем конфиг
            $this->_setConfig($app);
            // устанавливаем выбранную тему
            $this->_setTheme($app);
//            // Менюшки из базы
//            $this->_setMenu();
        }
    }

    /**
     * Устанавливает тему приложения из переменной self::$theme
     * @param \yii\web\Application $app
     * @return boolean
     */
    protected function _setTheme($app)
    {
        $app->view->theme = Yii::createObject([
            'class' => '\yii\base\Theme',
            'pathMap' => [
                'composer/module' => INSTANCE_PATH . "/themes/{$this->theme}/modules",
                '@app/views' => INSTANCE_PATH . "/themes/{$this->theme}",
                INSTANCE_PATH . '/views' => INSTANCE_PATH . "/themes/{$this->theme}",
                INSTANCE_PATH . '/modules' => INSTANCE_PATH . "/themes/{$this->theme}/modules",
                INSTANCE_PATH . '/widgets' => INSTANCE_PATH . "/themes/{$this->theme}/widgets"
            ],
            'baseUrl' => "@web/themes/{$this->theme}",
        ]);

        return true;
    }

    /**
     * Выполняем редирект (если он нужен)
     * @param $app \yii\web\Application
     */
    protected function _redirects($app)
    {
        $redirector = new \kupi_othodov_ru\module_main\components\Redirector($app->request->url);
        $redirector->run();
    }

    /**
     * Выбирает из базы все секции меню и добавляет в компонент меню
     */
    protected function _setMenu()
    {
        $sections = \kupi_othodov_ru\module_main\models\Menu::find()->active()->all();

        foreach ($sections as $section) {
            $items = $section->getMenuItems()->all();

            if (!empty($items)) {
                $result = [
                    'section' => $section->section,
                    'items' => $this->makeMenuData($items)
                ];
            }
        }

        if (!empty($result)) {
            \app\components\widgets\Menu::getInstance()->addItems($result);
        }
    }

    public function makeMenuData($items)
    {
        $result = [];
        foreach ($items as $item) {
            $menuItem = [
                'label' => $item->name,
                'url' => $item->url,
                'options' => (!empty($item->options)) ? $item->options : [],
                'linkOptions' => (!empty($item->linkOptions)) ? $item->linkOptions : [],
                'dropDownOptions' => (!empty($item->dropDownOptions)) ? $item->dropDownOptions : [],
            ];

            if (!empty($item->url)) {
                $menuItem[] = $item->url;
            }

            if (!empty($item->getChildren()->all())) {
                $menuItem['items'] = $this->makeMenuData($item->getChildren()->all());
            }

            $result[] = $menuItem;
        }
        return $result;
    }

    /**
     * Устанавливает конфиг, полученный из базы (ну или из корована)
     * @param \yii\web\Application $app
     */
    protected function _setConfig($app)
    {
        $config = \kupi_othodov_ru\module_main\components\SiteConfig::getConfig();

        if (isset($config['MAILER.FILE_TRANSPORT'])) {
            $mailer = \Yii::$app->mailer;
            $mailer->useFileTransport = (boolean)$config['MAILER.FILE_TRANSPORT'];

            if (!$mailer->useFileTransport) {
                $mailer->transport->setAuthMode('login');
                $mailer->transport->setHost($config['MAILER.HOST']);
                $mailer->transport->setPort($config['MAILER.PORT']);
                $mailer->transport->setEncryption($config['MAILER.ENCRYPTION']);
                $mailer->transport->setUsername($config['MAILER.USERNAME']);
                $mailer->transport->setPassword($config['MAILER.PASSWORD']);
            }
        }

        if (!empty($config['THEME'])) {
            $this->theme = $config['THEME'];
        }

        if (!empty($config['APP.NAME'])) {
            $app->name = $config['APP.NAME'];
        }

        foreach ($config as $key => $param) {
            if (strstr($key, 'PARAM'))
                $app->params[str_replace('PARAM.', '', $key)] = $param;
            if (strstr($key, 'MAILER'))
                $app->params[$key] = $param;
        }
    }
}