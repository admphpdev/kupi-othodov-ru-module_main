<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 05.11.2016
 * Time: 21:01
 */

namespace kupi_othodov_ru\module_main\controllers;


use amd_php_dev\yii2_components\controllers\PublicController;
use kupi_othodov_ru\module_catalog\models\Catalog;
use yii\base\Exception;
use yii\debug\DebugAsset;
use yii\log\Logger;
use yii\web\BadRequestHttpException;

class MailController extends \yii\web\Controller
{
    const CATALOG_CLASS = 'kupi_othodov_ru\module_catalog\models\Catalog';
    const RECAPTCHA_VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';
    const RECAPTCHA_RESPONSE_FIELD = 'g-recaptcha-response';

    public function actionCallBack()
    {
        if (!$this->checkReCaptcha()) {
            \Yii::$app->end();
        }
        $model = $this->createModel();

        $model->address = !empty(\Yii::$app->params['MAIL.CALLBACK']) ? \Yii::$app->params['MAIL.CALLBACK'] : $this->getAddress();

        if ($this->sendMail($model)) {
            $model->active = \kupi_othodov_ru\module_main\models\MainMail::ACTIVE_ACTIVE;
        } else {
            $model->active = \kupi_othodov_ru\module_main\models\MainMail::ACTIVE_BLOCKED;
        }

        $model->save();
        \Yii::$app->end();
    }

    public function actionQuestion()
    {
        $model = $this->createModel();

        $model->name = 'База знаний';
        $model->address = !empty(\Yii::$app->params['MAIL.QUESTION']) ? \Yii::$app->params['MAIL.QUESTION'] : $this->getAddress();

        if ($this->sendMail($model)) {
            $model->active = \kupi_othodov_ru\module_main\models\MainMail::ACTIVE_ACTIVE;
        } else {
            $model->active = \kupi_othodov_ru\module_main\models\MainMail::ACTIVE_BLOCKED;
        }

        $model->save();
        \Yii::$app->end();
    }

    public function actionFindError()
    {
        $model = $this->createModel();

        $model->name = 'Ошибка на сайте';
        $model->address = !empty(\Yii::$app->params['MAIL.ERROR']) ? \Yii::$app->params['MAIL.ERROR'] : $this->getAddress();

        if ($this->sendMail($model)) {
            $model->active = \kupi_othodov_ru\module_main\models\MainMail::ACTIVE_ACTIVE;
        } else {
            $model->active = \kupi_othodov_ru\module_main\models\MainMail::ACTIVE_BLOCKED;
        }

        $model->save();
        \Yii::$app->end();
    }

    /**
     * Отправка заказа
     */
    public function actionMakeOrder()
    {
        $model = $this->createOrderMailModel();

        $model->address = !empty(\Yii::$app->params['MAIL.CALLBACK']) ? \Yii::$app->params['MAIL.CALLBACK'] : $this->getAddress();

        if ($this->sendMail($model)) {
            $model->active = \kupi_othodov_ru\module_main\models\MainMail::ACTIVE_ACTIVE;
        } else {
            $model->active = \kupi_othodov_ru\module_main\models\MainMail::ACTIVE_BLOCKED;
        }

        $model->save();
        \Yii::$app->end();
    }

    /**
     * Создает модель письма на основе заказываемого товара
     */
    protected function createOrderMailModel()
    {
        $request = \Yii::$app->request;
        $formData = $request->post();

        if (!$this->checkForm($formData) || !isset($formData['catalogId'])) {
            throw new BadRequestHttpException('Неверно заполнена форма!');
        }

        $catalog = call_user_func([self::CATALOG_CLASS, 'find'])->byPk($formData['catalogId'])->one();

        if (!$catalog) {
            throw new \Exception('Неверный каталог.');
        }

        $date = date('h:i:s d.m.Y');
        $model = new \kupi_othodov_ru\module_main\models\MainMail();
        $model->name = 'Заказ';
        $model->page = $request->referrer;
        $model->address = $this->getAddress();
        $phone = isset($formData['phone']) ? "\nТелефон: {$formData['phone']};" : '';
        $text = <<<TEXT
Имя: {$formData['name']};
{$phone};
Время: {$date};
Страница: {$model->page};
Товар: #{$catalog->id} {$catalog->name};
Объем: {$formData['amount']}
TEXT;
        $model->data = \yii\helpers\HtmlPurifier::process($text);

        return $model;
    }

    /**
     * @return \kupi_othodov_ru\module_main\models\MainMail
     * @throws BadRequestHttpException
     */
    protected function createModel()
    {
        $request = \Yii::$app->request;
        $formData = $request->post();

        if (!$this->checkForm($formData)) {
            throw new BadRequestHttpException('Не верно заполнена форма!');
        }

        $date = date('h:i:s d.m.Y');
        $model = new \kupi_othodov_ru\module_main\models\MainMail();
        $model->name = 'Обратный звонок';
        $model->page = $request->referrer;
        $model->address = $this->getAddress();
        $email = isset($formData['email']) ? "\nПочта: {$formData['email']}" : '';
        $phone = isset($formData['phone']) ? "\nТелефон: {$formData['phone']};" : '';
        $text = <<<TEXT
Имя: {$formData['name']};{$email};{$phone}
Время: {$date};
Страница: {$model->page};
Вопрос:
{$formData['question']};
TEXT;
        $model->data = \yii\helpers\HtmlPurifier::process($text);

        return $model;
    }

    protected function checkForm($formData)
    {
        $res = true;

        if (empty($formData['name'])) {
            $res = false;
        }

//        if (empty($formData['email'])) {
//            $res = false;
//        }

//        if (empty($formData['question'])) {
//            $res = false;
//        }

        return $res;
    }

    /**
     * @param $mail \kupi_othodov_ru\module_main\models\MainMail
     * @return mixed
     */
    public function sendMail($mail)
    {
        try {
            $result = \Yii::$app->mailer->compose('@mainModule/mails/email-form', ['mail' => $mail])
                ->setFrom([\Yii::$app->params['MAILER.MAIL'] => \Yii::$app->name])
                ->setTo($mail->address)
                ->setSubject($mail->name . ' ' . \Yii::$app->request->hostName)
                ->send();
        } catch (Exception $e) {
            // $mail->data . "\n\n {$e->getMessage()}";
            $result = false;
        }

        return $result;
    }

    public function getAddress()
    {
        return \Yii::$app->params['SUPPORT_EMAIL'];
    }

    protected function checkReCaptcha()
    {
        if (!$recaptcha = \Yii::$app->request->post(self::RECAPTCHA_RESPONSE_FIELD)) {
            return false;
        }

        $secret = \Yii::$app->params['RECAPTCHA.S_KEY'];
        $res=$this->getCurlData(self::RECAPTCHA_VERIFY_URL, [
            'secret' => $secret,
            'response' => $recaptcha,
            'remoteip' => \Yii::$app->request->getUserIP(),
        ]);
        $res= json_decode($res, true);
        //reCaptcha введена
        return (boolean) $res['success'];
    }

    protected function getCurlData($url, $postFields)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
        $curlData = curl_exec($curl);
        curl_close($curl);
        return $curlData;
    }

}