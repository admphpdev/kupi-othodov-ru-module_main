<?php

namespace kupi_othodov_ru\module_main\models;

/**
 * This is the ActiveQuery class for [[Param]].
 *
 * @see Param
 */
class ParamQuery extends \amd_php_dev\yii2_components\models\SmartQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Param[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Param|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}