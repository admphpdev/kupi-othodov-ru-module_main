<?php

namespace kupi_othodov_ru\module_main\models;

use amd_php_dev\yii2_components\widgets\form\SmartInput;
use Yii;

/**
 * This is the model class for table "{{%main_meta_data}}".
 *
 * @property string $url
 * @property string $h1
 * @property string $metaDescription
 * @property string $text
 * @property string $metaTitle
 * @property integer $id
 * @property integer $active
 * @property integer $priority
 */
class MetaData extends \amd_php_dev\yii2_components\models\MetaData
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%main_meta_data}}';
    }

    /**
     * @inheritdoc
     * @return MetaDataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MetaDataQuery(get_called_class());
    }
}
